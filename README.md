# Gambit Scheme to Javascript by Example

This repository contains examples of using Gambit's awesome Javascript backend.
I started writing it while exploring and learning by myself how to interface
Scheme and Javascript code. Feel free to use these example as a starting point
for your own projects (License below).

## Pre-requisites

You need a recent version of Gambit installed, ideally 4.9.4+ or compiled
direcly from the master branch.

## Usage

Run `make` in the project directory and open with a web browser the
corresponding `index.html` file of the example you wish to explore.

## More information

The best documentation to date of Gambit's JS FFI is the paper
[A Scheme Foreign Function Interface to JavaScript Based on
an Infix Extension](https://www.iro.umontreal.ca/~feeley/papers/BelangerFeeleyELS21.pdf), which explains how the FFI works, and gives several
examples of it's usage.

## Contributing

It would be awesome to have examples showing how to use Gambit JS target in
several use cases. Pull requests are more than welcome.

## License

This code is licensed under the MIT license. See `LICENSE` for details.
