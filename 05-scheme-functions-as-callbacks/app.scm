(##include "~~lib/_six/js.sld")

(declare
 (extended-bindings)
 (standard-bindings)
 (separate))

(import (_six js)
        (gambit))

(define (hello ev)
  \alert('Hello World!'))

(let ((my-button \document.getElementById('myButton')))
  \(`my-button).addEventListener("click",`hello))

;;; The following line is needed to prevent the runtime system of shutting down.
;;; To understand this, here Marc Feeleys answer explaining the life cycle of a Scheme
;;; program running in the browser:
;;; "The program execution starts with the initialization of the Scheme runtime system,
;;; then the top-level of each Scheme module (ending with the main module) are executed,
;;; and then the Scheme runtime system shuts down and does whatever cleanup is required
;;; (this is analogous to an executable program executing in a standard operating system).
;;; The (thread-sleep! +inf.0) will prevent the shutdown of the runtime system so that
;;; the Scheme code of the event handler can continue executing."
(thread-sleep! +inf.0)
