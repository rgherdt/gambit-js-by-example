(##include "~~lib/_six/js.sld")

(declare
 (standard-bindings)
 (extended-bindings))

(import (_six js)
        (gambit))

;;; Gambit's SIX (Scheme Infix eXtension) provide a convenient way to mix Scheme
;;; and Javascript code. A backslash switches to infix notation, which is
;;; evaluated in the Javascript environment. A backquote switches back to prefix
;;; notation (which is always evaluated in the Scheme environment).
;;; See https://www-labs.iro.umontreal.ca/~feeley/papers/BelangerFeeleyELS21.pdf
;;; for more details.

(define (alert obj)
  \alert(`obj))

(define (prompt msg)
  \prompt(`(if (string? msg)
               msg
               (object->string msg))))

(alert (string-append "Hello "
                      (prompt "Enter your name:")
                      "!"))

;;; Alternatively, one can use infix notation directly:
;; \alert(`(string-append "Hello "
;;                        \prompt("Enter your name:")
;;                        "!"))
