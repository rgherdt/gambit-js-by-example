(##include "~~lib/_six/js.sld")

(declare
 (standard-bindings)
 (extended-bindings))

(import (_six js)
        (gambit))

(define (update-page elem obj)
  \(`elem).innerText=`obj)

(define (wait-and-update elem sec)
  ;;; calling update-page here fails (all calls are executed once). Any
  ;;; ideas why?
  \waitFor(`(- 11 sec)).then(function (r) { (`elem).innerText=`sec ;
                                            }))

(define (count-down)
  (let ((elem \document.getElementById('counter')))
    ;;; A thread blocks until the underlying promise is fulfilled.
    (map (lambda (sec)
           (thread (lambda () (wait-and-update elem sec))))
         (iota 11 10 -1))
    (update-page elem "Starting countdown")
    (thread-sleep! 12)
    (update-page elem "Launch")))


(for-each thread-join!
          (count-down))
