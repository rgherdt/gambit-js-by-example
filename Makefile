chapters = 01-hello-world \
           02-six-syntax \
           04-passing-js-values-to-scheme \
           05-scheme-functions-as-callbacks \
           06-react-hello-world \
           07-react-hello-world-six \
           08-snake-with-canvas \
           09-async-counter \
           10-websocket-client

app_sources = ${chapters:%=%/app.scm}
app_targets = ${chapters:%=%/app.js}

.PHONY: all clean

all: ${app_targets} 03-r7rs-libraries/app.js

lib/gambit-js-utils.js: lib/gambit-js-utils.scm
	gsc -warnings -c -target js -o $@ $<

${app_targets}: %.js: %.scm
	gsc -warnings -target js -o $@ -exe -nopreload $<

03-r7rs-libraries/app.js: 03-r7rs-libraries/app.scm
	gsc -warnings -target js -o $@ -exe -nopreload lib/ lib/gambit-js-utils.sld $<

clean:
	rm -Rf ${app_targets} lib/gambit-js-utils.js
	$(foreach i,$(chapters),rm -f $i/*.o*; rm -f $i/*.js)
	rm -f 03-r7rs-libraries/*.o* 03-r7rs-libraries/*.js
