(##include "~~lib/_six/js.sld")

(declare
 (standard-bindings)
 (extended-bindings))

(import (_six js)
        (gambit))

(define (alert msg)
  \alert(`msg))

(define (prompt msg)
  \prompt(`(if (string? msg)
               msg
               (object->string msg))))

(define server-address "ws://echo.websocket.events")

(define socket \new WebSocket(`server-address))

(define (on-open-callback ev)
  (alert (string-append "Connecting to " server-address))
  (let ((msg (prompt "Type a message")))
    \(`socket).send(`msg)))

\(`socket).onopen=`on-open-callback

(define (on-message-callback ev)
  (alert (string-append "Message received: "
                         \(`ev).data)))

\(`socket).onmessage=`on-message-callback

(define (on-close-callback ev)
  (alert "Connection closed"))

\(`socket).onclose=`on-close-callback

(define (on-error-callback err)
  (alert (string-append "Error: " err)))

\(`socket).onerror=`on-message-callback

(thread-sleep! +inf.0)
