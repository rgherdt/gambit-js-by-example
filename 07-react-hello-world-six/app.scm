(##include "~~lib/_six/js.sld")

(declare
 (standard-bindings)
 (extended-bindings))

(import (_six js))

(##inline-host-statement "\"use strict;\"")

;;; Some helper functions to easily create react components.

;; TODO: investigate a more elegant way of doing this
(define (h tag . args)
  (let ((len (length args)))
    (case len
      ((0) \React.createElement(`tag,null))
      ((1) \React.createElement(`tag,null,`(car args)))
      ((2) \React.createElement(`tag,null,`(car args),`(cadr args))))))

(define (react-render obj container)
  \ReactDOM.render(`obj,`container))

(react-render (h "table"
                 (h "thead"
                    (h "tr"
                       (h "th" "Column 1")
                       (h "th" "Column 2")))
                 (h "tbody"
                    (h "tr"
                       (h "td" "Hello")
                       (h "td" "World"))))
              \document.getElementById('app'))
