(##include "~~lib/_six/js.sld")

(declare
 (standard-bindings)
 (extended-bindings))

(import (_six js)
        (gambit))

;;;; Data types
(define-structure point x y)

;; we represent the snake as a list of points.
(define snake-head car)
(define snake-body cdr)

;;;; Globals
(define background-color "white")
(define border-color "black")
(define snake-color "green")
\gameboard=document.getElementById('gameboard');
\gameboard_ctx=gameboard.getContext('2d');

(define board-width
  \gameboard.width)
(define board-height
  \gameboard.height)
(define snake-part-size 10)

;;;; JS interaction and helper procedures

(define (clear-canvas)
  \gameboard_ctx.fillStyle=`background-color
  \gameboard_ctx.strokestyle=`border-color
  \gameboard_ctx.fillRect(0,0,gameboard.width,gameboard.height)
  \gameboard_ctx.strokeRect(0,0,gameboard.width,gameboard.height)
  )

(define (game-over)
  \gameboard_ctx.fillStyle="red"
  \gameboard_ctx.font="bold 16px Arial"
  \gameboard_ctx.fillText("Game Over",
                          `(- (/ board-width 2) 32),
                          `(/ board-height 2)))

(define (draw-snake-part part)
  (let ((x (point-x part))
        (y (point-y part)))
    \gameboard_ctx.fillStyle=`snake-color
    \gameboard_ctx.strokeStyle=`border-color
    \gameboard_ctx.fillRect(`x, `y, `snake-part-size, `snake-part-size)
    \gameboard_ctx.strokeRect(`x, `y, `snake-part-size, `snake-part-size)
    ))

(define (drop-right lis  k)
  (let ((orig-len (length lis)))
    (let loop ((len orig-len)
               (rem lis)
               (res '()))
      (cond ((= len k)
             (reverse res))
            (else (loop (- len 1)
                        (cdr rem)
                        (cons (car rem) res)))))))

;;;; Game state

(define snake-direction
  (make-parameter 'right))

(define next-direction
  (make-parameter 'right))

(define snake
  (map (lambda (p)
         (apply make-point p))
       '((300 200)
         (290 200)
         (280 200)
         (270 200)
         (260 200)
         (250 200))))

;;;; Game logic

(define (key-code->direction code)
  (cond ((= code 37) 'left)
        ((= code 39) 'right)
        ((= code 38) 'up)
        ((= code 40) 'down)
        (else #f)))

(define (change-direction! new-dir)
  (case new-dir
    ((left right) (case (snake-direction)
                    ((left right) #f)
                    (else (snake-direction new-dir))))
    ((up down) (case (snake-direction)
                 ((up down) #f)
                 (else (snake-direction new-dir))))
    (else (error "invalid new direction" new-dir))))

(define (handle-keypress ev)
  (let* ((code (##inline-host-expression "@scm2host@(@1@).keyCode" ev))
         (new-dir (key-code->direction code)))
    (when new-dir
      (next-direction new-dir))))

(define (self-collision?)
  (member (snake-head snake) (snake-body snake)))

(define (move-snake!)
  (let* ((head (snake-head snake))
         (head-x (point-x head))
         (head-y (point-y head))
         (new-head
          (case (snake-direction)
            ((right)
             (if (>= head-x board-width)
                 (make-point 0 head-y)
                 (make-point (+ head-x snake-part-size)
                             head-y)))
            ((left)
             (if (<= head-x 0)
                 (make-point (- board-width snake-part-size) head-y)
                 (make-point (- head-x snake-part-size)
                             head-y)))
            ((up)
             (if (<= head-y 0)
                 (make-point head-x (- board-height snake-part-size))
                 (make-point head-x
                             (- head-y snake-part-size))))
            ((down)
             (if (>= head-y board-height)
                 (make-point head-x 0)
                 (make-point head-x
                             (+ head-y snake-part-size)))))))
    (set! snake
          (cons new-head
                (drop-right snake 1)))))

(define (draw-snake)
  (map draw-snake-part snake))


(define (init)
 (##inline-host-statement
  "var t = @scm2host@(@1@);
  document.addEventListener(\"keydown\", function (e) { t(e); })"
  handle-keypress))

(define (main)
  (let loop ()
    (clear-canvas)
    (change-direction! (next-direction))
    (move-snake!)
    (cond ((self-collision?)
           (game-over))
          (else (draw-snake)
                (thread-sleep! 0.3)
                (loop)))))

(init)
(main)
