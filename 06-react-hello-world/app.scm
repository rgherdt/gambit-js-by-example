(declare
 (extended-bindings))

(##inline-host-statement "\"use strict;\"")

;;; Some helper functions to easily create react components.

;; TODO: investigate why case-lambda is not working
(define (h tag . args)
  (if (not (null? args))
      (##inline-host-expression
       "@host2scm@(React.createElement(@scm2host@(@1@), null, @scm2host@(@2@)))"
       tag
       (car args))
      (##inline-host-expression
       "@host2scm@(React.createElement(@scm2host@(@1@), null))"
       tag)))

(define (react-render obj container)
  (##inline-host-statement
   "@host2scm@(ReactDOM.render(@scm2host@(@1@), @scm2host@(@2@)))"
   obj
   container))

(define (document.getElementById tag)
  (##inline-host-expression "@host2scm@(document.getElementById(@scm2host@(@1@)))" tag))

(react-render (h "div" "Hello") (document.getElementById "app"))
