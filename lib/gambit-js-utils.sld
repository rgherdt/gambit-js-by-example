(define-library (gambit-js-utils)

(export alert
        prompt
        fact)

(import (except (gambit) six.infix)
        (_six js)
        )

(begin
  (declare
   (extended-bindings) ;; to use ##inline-host-XXX
   (standard-bindings)
   (separate)
   )
  
  (define (alert obj)
    \alert(`obj)
    )
  (define (prompt msg)
    \prompt(`(if (string? msg) msg (object->string msg))))
  ))
